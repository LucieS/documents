﻿2018-06-05
přítomni: Zbyněk, Radomír, Dan, Láďa, Ondra, Marcel
agenda:
    členská schůze - místo a program
    odměna Dana
    
zápis:
    členská schůze
    • Hlasování: Výbor pověřuje Ondřeje Profanta zastupováním spolku ve věci zajištění prostor konání členské schůze na radnici Praha 5
    • pro: Zbyněk, Radomír, Láďa, Ondra
    • proti, zdržel se: nikdo
    • Program:
    • mělo by v něm být: 
    • Zpráva o hospodaření 
    • Volba předsedy: Dan pošle e-mail do konference, 280kč/hod, pozadavky viz stanovy
    • odměna Dana Koláře:
    • návrh usnesení: "Výbor schvaluje odměnu Dana Koláře ve výši 250 Kč za hodinu s platností od 1.1.2018."
    • protinávrh Zbyňka: "Výbor schvaluje odměnu Dana Koláře ve výši 275 Kč za hodinu s platností od 1.1.2018."
    • pro: Zbyněk
    • proti: nikdo
    • zdržel se: Marcel, Radomír, Ondřej, Laďa
    • Usnesení nebylo přijato.
    • návrh usnesení: "Výbor schvaluje odměnu Dana Koláře ve výši 250 Kč za hodinu s platností od 1.1.2018."
    • pro: Marcel, Laďa, Ondřej, Radomír
    • proti: Zbyněk
    • zdržel se: nikdo
    • Usnesení bylo přijato.

