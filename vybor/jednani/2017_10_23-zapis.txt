přítomni: Marcel, Laďa, Ondřej, Zbyněk, Dan
agenda:
1. hlasování per rollam
2. stav úkolů
* Laďa rozešle výzvy k zaplacení členských příspěvků do 31.8.2017
o Marcel rozeslal, je třeba vystavit v gitu
* Marcel domluví s účetním použití tagů ve FlexiBee
* public money, public code: Ondřej pošle do konference
3. zveřejnění zápisu z členské schůze
4. zápis změny obsazení výboru do restříku
5. p. Hazuza: žádost o členství
6. přednášky na OpenAlt
7. registr smluv
8. newsletter, novinky, aktuality
9. Call for Digital Innovation Hubs - určitě pro, potřebujeme projektanta
10. Clenstvi asociace v otevrenych mestech
11. nastavení přeposílání mailů z Cityvizor mailu na Danův soukromý mail 
12. aktualizace dat Nového Města na Moravě pro CityVizor
13. ostatní
* open data - Brno (SLA) | katalog městských dat  https://kmd.otevrenamesta.cz/ a katalog otevřených dat https://ckan.otevrenamesta.cz/
* https://monitor.otevrenamesta.cz/  https://diskurz.otevrenamesta.cz/
zápis:
1. hlasování per rollam
2. stav úkolů
* Laďa rozešle výzvy k zaplacení členských příspěvků do 31.8.2017
o Marcel rozeslal, je třeba vystavit v gitu
o AI: Laďa vystaví odeslané datové zprávy v gitu
* Marcel domluví s účetním použití tagů ve FlexiBee
* public money, public code: Ondřej pošle do konference
* Marcel zřídí přístup Danovi do datové schránky
3. zveřejnění zápisu z členské schůze
* AI: Laďa zveřejní
4. zápis změny obsazení výboru do restříku
* připraví Dan, jakmile mu Marcel zřídí přístup do datovky
5. p. Hazuza: žádost o členství
* AI: Laďa promluví s p. Hazuzou
6. přednášky na OpenAlt
* Cityvizor
* Otevřená města 2018: neděle 12:30
o AI: Laďa pošle pozvánku do konference
7. registr smluv
* připomínky členů jsou zde:
o https://pad.openalt.org/om_NMNM_2017
o AI: Zbyněk do konce října předloží návrh nové funkcionality evidence smluv BISON, kterou dále budeme implementovat pomocí modulů
8. newsletter, novinky, aktuality
* AI: Dan vytvoří draft do 29.10.
9. Call for Digital Innovation Hubs - určitě pro, potřebujeme projektanta
* deadline 7.11. => nereálné
10. Clenstvi asociace pro elektromobilitu v otevrenych mestech
* AI: Ondřej pošle e-mailem
11. nastavení přeposílání mailů z Cityvizor mailu na Danův soukromý mail
* AI: Laďa nastaví alias cityvizor@otevrenemesta.cz na Dana
12. aktualizace dat Nového Města na Moravě pro CityVizor
* AI: Ondřej pořeší
13. ostatní
* open data - Brno (SLA) | katalog městských dat  https://kmd.otevrenamesta.cz/ a katalog otevřených dat https://ckan.otevrenamesta.cz/
o AI: Laďa probere s Brnem
* https://monitor.otevrenamesta.cz/  https://diskurz.otevrenamesta.cz/

