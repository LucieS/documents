﻿2018-11-07

avizovaná neúčast:  Marcel
přítomni: Laďa, Ondra, Radomír, Zbyněk, Pavla

Agenda:
• členský příspěvek Brno
• Praha 9 (CV resp členství)
• web P7
• spolupráce s VŠE
• asistent CV
• newsletter
• OpenAlt

Zápis:
• Prezentace OpenAlt: https://docs.google.com/presentation/d/1gRbuNJkDvRxzHFgbittZI6zfqUuTbH_aOTNrt65j2uU/edit?usp=sharing
• plánovaný vstup Ostravy i všech městských částí i částí Prahy
• KISK - výjezdní zasedání na přelomu listopad a prosinec. Byla by dobrá naše účast
• Praha 7 web  - volat inCool
	o zkombinovat s P5, kde mají nově také WP a třeba plugin pro vyvolávací systém
• spolupráce s VŠE - kukátko do státní pokladny není user friendly a ministerstvo nespolupracuje - bere si na starost Radomír
• asistent CV - přes 50 uchazečů, 30 lidí do dalšího kola a pak 4 finalisté, finální volba Pavla Kadlecová
• newsletter - má na starosti Ondra
• členský příspěvek Brno - Laďa bude dál referovat
• CityVizor - drhne v Damažlicích, Černošice nenají aktuální faktury, - nemají open data - proto nejsou aktuální data
• projektová rada - Domažlice?
• LibreOffice - u  RTF - průběžná stránka - problém - řeší Zbyněk
• v NMnM výběrko na SW pro Městskou policii
• evidence smluv - Ondřej - do 14 dnů pro Otevřená města - jen evidenční fce
• router pro malé obce - zmínit v newsletteru
• Plzeň - centralizovaný seznam dětí přihlášených do MŠ, ale bez  výstupu zpět pro školky, Brno to má dořešené a doplněné i o ZŠ
