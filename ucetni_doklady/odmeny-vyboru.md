Odměny členů Výboru Otevřených měst jsou propláceny na základě smlouvy, jejiž [vzor schválila členská schůze][smlouva-o-vykonu] jako bod [6 schůze 2016-08-09][zapis-2016-08-09]. Smlouvy členů výboru:

- [Marcel Kolaja][smlouva-kolaja]
- [Ladislav Nešnera][smlouva-nesnera]
- [Ondřej Profant][smlouva-profant]

Práce se vykazuje dle lánku 3 - "Funkcionář je povinen vykazovat čas strávený výkonem funkce v informačním systému organizace" v systému [redmine][]. Dotyční v období od podepsání smlouvy do 21. 4. 2019 vykázali:

| člen výboru      | 2016   | 2017   | 2018    | 2019*  | Celkem za období |
|------------------|-------:|-------:|--------:|-------:|------------------|
| Marcel Kolaja    | 014.75 | 090.50 |  041.50 | 003.50 | |
| Ladislav Nešnera | 133.50 | 174.75 | 1029.75 | 348.50 | |
| Ondřej Profant   | 008.10 | 004.50 |    0.00 |   0.00 | 0012.60 |

\* Z roku 2019 pouze do 21. 4. 2019.

Odměna na základě funkce jednatele podléhá zdanění včetně odvodů na ZP a SP (je to stejné jako u klasické mzdy). Z hrubé mzdy by se tak platilo 25 % na SP a 9 % na ZP (to by platila Otevřená města). Z Vaší odměny by se odvedlo 6,5 % na SP a 4,5 % na ZP. Daň by se platila se superhrubé mzdy.

[smlouva-o-vykonu]: https://gitlab.com/otevrenamesta/documents/blob/master/vzory/smlouva_o_vykonu_funkce.odt
[zapis-2016-08-09]: https://gitlab.com/otevrenamesta/documents/blob/master/schuze/2016_08_09/zapis_2016_08_09.pdf
[smlouva-nesnera]: https://gitlab.com/otevrenamesta/documents/blob/master/smlouvy/2016_09_25-vykon_funkce-Ladislav_Nesnera/smlouva_o_vykonu_funkce.pdf
[smlouva-kolaja]: https://gitlab.com/otevrenamesta/documents/blob/master/smlouvy/2016_09_25-vykon_funkce-Marcel_Kolaja/smlouva_o_vykonu_funkce.pdf
[smlouva-profant]: https://gitlab.com/otevrenamesta/documents/blob/master/smlouvy/2016_09_01-vykon_funkce-Ondrej_Profant/smlouva_o_vykonu_funkce.pdf
[redmine]: http://ukoly.openalt.org/projects/om
[nesnera-2016]: http://ukoly.openalt.org/projects/om/time_entries?utf8=%E2%9C%93&f%5B%5D=spent_on&op%5Bspent_on%5D=%3E%3C&v%5Bspent_on%5D%5B%5D=2016-08-01&v%5Bspent_on%5D%5B%5D=2016-12-31&f%5B%5D=user_id&op%5Buser_id%5D=%3D&v%5Buser_id%5D%5B%5D=4&f%5B%5D=&c%5B%5D=project&c%5B%5D=spent_on&c%5B%5D=user&c%5B%5D=activity&c%5B%5D=issue&c%5B%5D=comments&c%5B%5D=hours
[kolaja-2017]: http://ukoly.openalt.org/projects/om/time_entries?utf8=%E2%9C%93&f%5B%5D=spent_on&op%5Bspent_on%5D=%3E%3C&v%5Bspent_on%5D%5B%5D=2017-01-01&v%5Bspent_on%5D%5B%5D=2017-12-31&f%5B%5D=user_id&op%5Buser_id%5D=%3D&v%5Buser_id%5D%5B%5D=130&f%5B%5D=&c%5B%5D=project&c%5B%5D=spent_on&c%5B%5D=user&c%5B%5D=activity&c%5B%5D=issue&c%5B%5D=comments&c%5B%5D=hours
[nesnera-2017]: http://ukoly.openalt.org/projects/om/time_entries?utf8=%E2%9C%93&f%5B%5D=spent_on&op%5Bspent_on%5D=%3E%3C&v%5Bspent_on%5D%5B%5D=2017-01-01&v%5Bspent_on%5D%5B%5D=2017-12-31&f%5B%5D=user_id&op%5Buser_id%5D=%3D&v%5Buser_id%5D%5B%5D=4&f%5B%5D=&c%5B%5D=project&c%5B%5D=spent_on&c%5B%5D=user&c%5B%5D=activity&c%5B%5D=issue&c%5B%5D=comments&c%5B%5D=hours
[profant-2017]: http://ukoly.openalt.org/projects/om/time_entries?utf8=%E2%9C%93&f%5B%5D=spent_on&op%5Bspent_on%5D=%3E%3C&v%5Bspent_on%5D%5B%5D=2017-01-01&v%5Bspent_on%5D%5B%5D=2017-12-31&f%5B%5D=user_id&op%5Buser_id%5D=%3D&v%5Buser_id%5D%5B%5D=136&f%5B%5D=&c%5B%5D=project&c%5B%5D=spent_on&c%5B%5D=user&c%5B%5D=activity&c%5B%5D=issue&c%5B%5D=comments&c%5B%5D=hours
[kolaja-2018]: http://ukoly.openalt.org/projects/om/time_entries?utf8=%E2%9C%93&f%5B%5D=spent_on&op%5Bspent_on%5D=%3E%3C&v%5Bspent_on%5D%5B%5D=2018-01-01&v%5Bspent_on%5D%5B%5D=2018-12-31&f%5B%5D=user_id&op%5Buser_id%5D=%3D&v%5Buser_id%5D%5B%5D=130&f%5B%5D=&c%5B%5D=project&c%5B%5D=spent_on&c%5B%5D=user&c%5B%5D=activity&c%5B%5D=issue&c%5B%5D=comments&c%5B%5D=hours
[nesnera-2018]: http://ukoly.openalt.org/projects/om/time_entries?utf8=%E2%9C%93&f%5B%5D=spent_on&op%5Bspent_on%5D=%3E%3C&v%5Bspent_on%5D%5B%5D=2018-01-01&v%5Bspent_on%5D%5B%5D=2018-12-31&f%5B%5D=user_id&op%5Buser_id%5D=%3D&v%5Buser_id%5D%5B%5D=4&f%5B%5D=&c%5B%5D=project&c%5B%5D=spent_on&c%5B%5D=user&c%5B%5D=activity&c%5B%5D=issue&c%5B%5D=comments&c%5B%5D=hours
[profant-2018]: http://ukoly.openalt.org/projects/om/time_entries?utf8=%E2%9C%93&f%5B%5D=spent_on&op%5Bspent_on%5D=%3E%3C&v%5Bspent_on%5D%5B%5D=2018-01-01&v%5Bspent_on%5D%5B%5D=2018-12-31&f%5B%5D=user_id&op%5Buser_id%5D=%3D&v%5Buser_id%5D%5B%5D=136&f%5B%5D=&c%5B%5D=project&c%5B%5D=spent_on&c%5B%5D=user&c%5B%5D=activity&c%5B%5D=issue&c%5B%5D=comments&c%5B%5D=hours
[kolaja-2019]: http://ukoly.openalt.org/projects/om/time_entries?utf8=%E2%9C%93&f%5B%5D=spent_on&op%5Bspent_on%5D=%3E%3C&v%5Bspent_on%5D%5B%5D=2019-01-01&v%5Bspent_on%5D%5B%5D=2019-04-21&f%5B%5D=user_id&op%5Buser_id%5D=%3D&v%5Buser_id%5D%5B%5D=130&f%5B%5D=&c%5B%5D=project&c%5B%5D=spent_on&c%5B%5D=user&c%5B%5D=activity&c%5B%5D=issue&c%5B%5D=comments&c%5B%5D=hours
[nesnera-2019]: http://ukoly.openalt.org/projects/om/time_entries?utf8=%E2%9C%93&f%5B%5D=spent_on&op%5Bspent_on%5D=%3E%3C&v%5Bspent_on%5D%5B%5D=2019-01-01&v%5Bspent_on%5D%5B%5D=2019-04-21&f%5B%5D=user_id&op%5Buser_id%5D=%3D&v%5Buser_id%5D%5B%5D=4&f%5B%5D=&c%5B%5D=project&c%5B%5D=spent_on&c%5B%5D=user&c%5B%5D=activity&c%5B%5D=issue&c%5B%5D=comments&c%5B%5D=hours